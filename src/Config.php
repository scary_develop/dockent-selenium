<?php

namespace Dockent\Selenium;

/**
 * Class Config
 * @package Dockent\Selenium
 */
class Config
{
    /**
     * @var array
     */
    private static $data;

    /**
     * @return array
     */
    public static function get(): array
    {
        if (static::$data === null) {
            static::$data = parse_ini_file(__DIR__ . '/config.ini', true);
        }
        return static::$data;
    }
}