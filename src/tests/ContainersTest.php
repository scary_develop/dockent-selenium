<?php

namespace Dockent\Selenium\tests;

use Dockent\Selenium\Config;
use Dockent\Selenium\selectors\ContainerCreateXPath;
use Dockent\Selenium\selectors\GeneralXPath;
use Dockent\Selenium\SeleniumInstance;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use PHPUnit\Framework\TestCase;

/**
 * Class ContainersTest
 * @package Dockent\Selenium\tests
 */
class ContainersTest extends TestCase
{
    /**
     * @var RemoteWebDriver
     */
    private $driver;

    protected function setUp()
    {
        $this->driver = SeleniumInstance::get();
    }

    protected function testCreate()
    {
        $this->driver->get(Config::get()['project']['host']);
        $this->driver->findElement(WebDriverBy::xpath(GeneralXPath::SIDEBAR_CONTAINERS_CREATE))->click();

        $this->driver->findElement(WebDriverBy::xpath(ContainerCreateXPath::IMAGE))
            ->sendKeys('ubuntu:latest');
        $this->driver->findElement(WebDriverBy::xpath(ContainerCreateXPath::CMD))
            ->sendKeys('bash');
        $this->driver->findElement(WebDriverBy::xpath(ContainerCreateXPath::NAME))
            ->sendKeys('selenium_test_container');

        $this->driver->findElement(WebDriverBy::xpath(ContainerCreateXPath::SUBMIT))->click();
    }
}