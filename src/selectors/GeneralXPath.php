<?php

namespace Dockent\Selenium\selectors;

/**
 * Class General
 * @package Dockent\Selenium\selectors
 */
abstract class GeneralXPath
{
    const PROJECT_TITLE = '//*[@id="root"]/div/h2/div';
    const SIDEBAR_DASHBOARD = '//*[@id="root"]/div/div[1]/div[1]/div/div[1]/div/a';
    const SIDEBAR_CONTAINERS = '//*[@id="root"]/div/div[1]/div[1]/div/div[2]/div[1]';
    const SIDEBAR_CONTAINERS_LIST = '//*[@id="root"]/div/div[1]/div[1]/div/div[2]/div[2]/div[1]/a';
    const SIDEBAR_CONTAINERS_CREATE = '//*[@id="root"]/div/div[1]/div[1]/div/div[2]/div[2]/div[2]/a';
    const SIDEBAR_IMAGES = '//*[@id="root"]/div/div[1]/div[1]/div/div[3]/div[1]';
    const SIDEBAR_IMAGES_LIST = '//*[@id="root"]/div/div[1]/div[1]/div/div[3]/div[2]/div[1]/a';
    const SIDEBAR_IMAGES_BUILD = '//*[@id="root"]/div/div[1]/div[1]/div/div[3]/div[2]/div[2]/a';
    const SIDEBAR_NETWORK = '//*[@id="root"]/div/div[1]/div[1]/div/div[4]/div[1]';
    const SIDEBAR_NETWORK_LIST = '//*[@id="root"]/div/div[1]/div[1]/div/div[4]/div[2]/div[1]/a';
    const SIDEBAR_NETWORK_CREATE = '//*[@id="root"]/div/div[1]/div[1]/div/div[4]/div[2]/div[2]/a';
    const SIDEBAR_SETTINGS = '//*[@id="root"]/div/div[1]/div[1]/div/div[5]/div/a';
}