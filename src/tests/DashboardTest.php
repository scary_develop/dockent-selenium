<?php

namespace Dockent\Selenium\tests;

use Dockent\Selenium\Config;
use Dockent\Selenium\selectors\DashboardXPath;
use Dockent\Selenium\SeleniumInstance;
use Dockent\Selenium\values\SystemInfo;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use PHPUnit\Framework\TestCase;

/**
 * Class DashboardTest
 * @package Dockent\Selenium\tests
 */
class DashboardTest extends TestCase
{
    /**
     * @var RemoteWebDriver
     */
    private $driver;

    protected function setUp()
    {
        $this->driver = SeleniumInstance::get();
    }

    public function testGeneral()
    {
        $this->driver->get(Config::get()['project']['host']);
        $this->assertEquals(SystemInfo::KERNEL_VERSION, $this->driver
            ->findElement(WebDriverBy::xpath(DashboardXPath::SYS_INFO_KERNEL_VERSION))->getText());
        $this->assertEquals(SystemInfo::OPERATING_SYSTEM, $this->driver
            ->findElement(WebDriverBy::xpath(DashboardXPath::SYS_INFO_OPERATING_SYSTEM))->getText());
        $this->assertEquals(SystemInfo::ARCHITECTURE, $this->driver
            ->findElement(WebDriverBy::xpath(DashboardXPath::SYS_INFO_ARCHITECTURE))->getText());
        $this->assertEquals(SystemInfo::CPUS, $this->driver
            ->findElement(WebDriverBy::xpath(DashboardXPath::SYS_INFO_CPUS))->getText());
        $this->assertEquals(SystemInfo::TOTAL_MEMORY, $this->driver
            ->findElement(WebDriverBy::xpath(DashboardXPath::SYS_INFO_TOTAL_MEMORY))->getText());
    }
}