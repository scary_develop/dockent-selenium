<?php

namespace Dockent\Selenium\values;

/**
 * Class Sidebar
 * @package Dockent\Selenium\values
 */
abstract class Sidebar
{
    const DASHBOARD = 'Dashboard';
    const CONTAINERS = 'Containers';
    const CONTAINERS_LIST = 'List';
    const CONTAINERS_CREATE = 'Create';
    const IMAGES = 'Images';
    const IMAGES_LIST = 'List';
    const IMAGES_BUILD = 'Build';
    const NETWORK = 'Network';
    const NETWORK_LIST = 'List';
    const NETWORK_CREATE = 'Create';
    const SETTINGS = 'Settings';
}