<?php

namespace Dockent\Selenium\values;

/**
 * Class SystemInfo
 * @package Dockent\Selenium\values
 */
abstract class SystemInfo
{
    const KERNEL_VERSION = 'Kernel version';
    const OPERATING_SYSTEM = 'Operating system';
    const ARCHITECTURE = 'Architecture';
    const CPUS = 'CPUs';
    const TOTAL_MEMORY = 'Total memory';
}