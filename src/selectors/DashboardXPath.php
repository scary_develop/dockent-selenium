<?php

namespace Dockent\Selenium\selectors;

/**
 * Class DashboardXPath
 * @package Dockent\Selenium\selectors
 */
abstract class DashboardXPath
{
    const SYS_INFO_KERNEL_VERSION = '//*[@id="root"]/div/div[1]/div[2]/div/div/div[2]/div[2]/div/div[1]/div/div';
    const SYS_INFO_OPERATING_SYSTEM = '//*[@id="root"]/div/div[1]/div[2]/div/div/div[2]/div[2]/div/div[2]/div/div';
    const SYS_INFO_ARCHITECTURE = '//*[@id="root"]/div/div[1]/div[2]/div/div/div[2]/div[2]/div/div[3]/div/div';
    const SYS_INFO_CPUS = '//*[@id="root"]/div/div[1]/div[2]/div/div/div[2]/div[2]/div/div[4]/div/div';
    const SYS_INFO_TOTAL_MEMORY = '//*[@id="root"]/div/div[1]/div[2]/div/div/div[2]/div[2]/div/div[5]/div/div';
}