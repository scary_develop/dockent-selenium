<?php

namespace Dockent\Selenium\tests;

use Dockent\Selenium\Config;
use Dockent\Selenium\selectors\GeneralXPath;
use Dockent\Selenium\SeleniumInstance;
use Dockent\Selenium\values\Common;
use Dockent\Selenium\values\Sidebar;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use PHPUnit\Framework\TestCase;

/**
 * Class GeneralTest
 * @package Dockent\Selenium\tests
 */
class GeneralTest extends TestCase
{
    /**
     * @var RemoteWebDriver
     */
    private $driver;

    protected function setUp()
    {
        $this->driver = SeleniumInstance::get();
    }

    public function testGeneral()
    {
        $this->driver->get(Config::get()['project']['host']);
        $this->assertEquals(Common::PROJECT_TITLE, $this->driver
            ->findElement(WebDriverBy::xpath(GeneralXPath::PROJECT_TITLE))->getText());

        $this->assertEquals(Sidebar::DASHBOARD, $this->driver
            ->findElement(WebDriverBy::xpath(GeneralXPath::SIDEBAR_DASHBOARD))->getText());
        $this->assertEquals(Sidebar::CONTAINERS, $this->driver
            ->findElement(WebDriverBy::xpath(GeneralXPath::SIDEBAR_CONTAINERS))->getText());
        $this->assertEquals(Sidebar::CONTAINERS_LIST, $this->driver
            ->findElement(WebDriverBy::xpath(GeneralXPath::SIDEBAR_CONTAINERS_LIST))->getText());
        $this->assertEquals(Sidebar::CONTAINERS_CREATE, $this->driver
            ->findElement(WebDriverBy::xpath(GeneralXPath::SIDEBAR_CONTAINERS_CREATE))->getText());
        $this->assertEquals(Sidebar::IMAGES, $this->driver
            ->findElement(WebDriverBy::xpath(GeneralXPath::SIDEBAR_IMAGES))->getText());
        $this->assertEquals(Sidebar::IMAGES_LIST, $this->driver
            ->findElement(WebDriverBy::xpath(GeneralXPath::SIDEBAR_IMAGES_LIST))->getText());
        $this->assertEquals(Sidebar::IMAGES_BUILD, $this->driver
            ->findElement(WebDriverBy::xpath(GeneralXPath::SIDEBAR_IMAGES_BUILD))->getText());
        $this->assertEquals(Sidebar::NETWORK, $this->driver
            ->findElement(WebDriverBy::xpath(GeneralXPath::SIDEBAR_NETWORK))->getText());
        $this->assertEquals(Sidebar::NETWORK_LIST, $this->driver
            ->findElement(WebDriverBy::xpath(GeneralXPath::SIDEBAR_NETWORK_LIST))->getText());
        $this->assertEquals(Sidebar::NETWORK_CREATE, $this->driver
            ->findElement(WebDriverBy::xpath(GeneralXPath::SIDEBAR_NETWORK_CREATE))->getText());
        $this->assertEquals(Sidebar::SETTINGS, $this->driver
            ->findElement(WebDriverBy::xpath(GeneralXPath::SIDEBAR_SETTINGS))->getText());
    }
}