<?php

namespace Dockent\Selenium\values;

/**
 * Class Common
 * @package Dockent\Selenium\values
 */
abstract class Common
{
    const PROJECT_TITLE = 'Dockent';
}