<?php

namespace Dockent\Selenium\selectors;

/**
 * Class ContainerCreateXPath
 * @package Dockent\Selenium\selectors
 */
abstract class ContainerCreateXPath
{
    const IMAGE = '//*[@id="root"]/div/div[1]/div[2]/div/form/div[2]/div/div/input';
    const CMD = '//*[@id="root"]/div/div[1]/div[2]/div/form/div[3]/div/div/input';
    const NAME = '//*[@id="root"]/div/div[1]/div[2]/div/form/div[4]/div/div/input';
    const SUBMIT = '//*[@id="root"]/div/div[1]/div[2]/div/form/button';
}