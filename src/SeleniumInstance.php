<?php

namespace Dockent\Selenium;

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;

/**
 * Class SeleniumInstance
 * @package Dockent\Selenium
 */
class SeleniumInstance
{
    /**
     * @var RemoteWebDriver
     */
    private static $driver;

    /**
     * @return RemoteWebDriver
     */
    public static function get(): RemoteWebDriver
    {
        if (static::$driver === null) {
            $host = Config::get()['selenium']['host'];
            static::$driver = RemoteWebDriver::create($host, DesiredCapabilities::firefox());
        }
        return static::$driver;
    }
}